class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  before_action :authenticate_user! #before any action performed by application controller, we need a signed up user who is also logged in
end
